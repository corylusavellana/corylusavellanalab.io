---
title: Comandos 'git' para un blog 'jekyll' en GitLab
author: ign
date: 2021-12-06 +0800
categories: [Computación, Software, Sistemas Operativos, redes]
tags: [ssh, remoto, redes, conexiones, administracion, seguridad, linux, software libre, cli, terminal, telnet, bash, comunicaciones]
render_with_liquid: false
pin: false
math: false
mermaid: false
toc: true
comments: false
---
## Comandos de preparación del repositorio git local
Desde la terminal hay que ir al directorio donde se ubicará el blog "en local", por ejemplo '/home/usuario/blogs/'. <small>(*) No hace falta crear ningún subdirectorio "extra" porque esto se hará automáticamente cuando se ejecute el comando git de clonación del repositorio remoto en el repositorio local.</small>
~~~
cd /home/usuario/blogs/
~~~
Es necesario conocer **la url del repositorio remoto** de nuestro blog (que está en gitlab.com). Esto se hace así:  
Projects → Your Projects →*usuario*/*nombredelproyecto* ...  
... → "Clone" → Clone with https  
<br>
@@@@@![URL del repositorio git remoto]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_url_repositorio_git.png)  
<small>URL del repositorio git remoto</small>  
<br>
Conocida la URL del repositorio remoto hay que clonarlo en nuestro directorio local para que se pueda ir trabajando "en local" y subir cuando interese los cambios al remoto. La clonación se realiza así:
~~~bash
git clone urldelrepositorioremoto
~~~
Por ejemplo: git clone https://gitlab.com/*usuariogitlab*/*usuariogitlab*.gitlab.io.git  
Se observa entonces que aparece un subdirectorio nuevo ('/usuariogitlab.gitlab.io').  
Hay que ubicarse, desde la terminal, en ese subdirectorio que acaba de aparecer y que es un espejo (clon) del repositorio remoto que tenemos en GitLab.
~~~bash
cd /home/usuario/blogs/usuariogitlab.gitlab.io
~~~
A continuación hay que inicializar el repositorio git en esa ubicación  
~~~bash
git init
~~~
El sistema habrá de responder:  
> Inicializado repositorio Git vacío en /home/usuario/blogs/usuariodegitlab.gitlab.io/.git/  

Hay que configurar entonces el acceso, la "identidad" para ese repositorio local que es espejo del remoto:  
~~~bash
git config user.name "usuariodegitlab"
git config user.email "dirección_de_email"
git config --list
~~~

<br>
## Comandos de sincronización
Con:
~~~bash
git status
~~~
... consultamos el estado de "lo local" respecto a "lo remoto".  

Cuando se realizan modificaciones "en repositorio **local**" y se desea "empujarlas" al "repositorio **remoto**" para que ambos estén sincronizados basta con ejecutar:
~~~bash
git add .
git commit -m "texto explicativo de los cambios que empujamos"
git push -u origin master
git status
~~~
Y cuando se realizan modificaciones "en repositorio **remoto**" y se desea traerlas también al "repositorio **local**" para que ambos estén sincronizados:
~~~bash
git pull origin master
~~~

<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://hijosdeinit.gitlab.io/creaci%C3%B3n-blog-jekyll-en-gitlab/">hijosdeinit.gitlab.io</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
