---
title: MineTest vol.01
author: ign
date: 2021-11-28 +0800
categories: [Computación, Software, Juegos, Minetest]
tags: [minetest, minecraft, voxel, juegos, linux, software libre, gpl, lua, pixel]
render_with_liquid: false
pin: false
math: false
mermaid: false
toc: true
comments: false
---
[Minetest](https://www.minetest.net/) es un motor de juego de tipo voxel escrito desde cero en C++ y licenciado bajo la LPGL (versión 2.1 o anterior). Pone a disposición del usuario una fácil creación de juegos y mods usando el lenguaje de programación [Lua](https://www.lua.org/), y viene junto con un "Juego de Minetest" que es similar a Minecraft. Minetest soporta los modos creativo y supervivencia, ambos con soporte multijugador, iluminación dinámica y un generador de mapas "infinitos".  

<br>
![Minetest](/assets/img/2021-11-29-Minetest_vol01/minetest.jpg)  
<small>'MineTest'</small>  
<br>

'Minetest' está en los repositorios de muchas distribuciones GNU Linux como, por ejemplo, 'Debian'.  
Asimismo, 'Minetest' también dispone de un [repositorio en 'GitHub'](https://github.com/minetest).  
En los repositorios de Debian existen varios paquetes relacionados con Minetest, listos para instalar según lo que se necesite.
- `minetest`: el juego y sus datos (desconozco aún si trae el servidor por defecto)
- `minetest-server`: el servidor de Minetest; se puede instalar independientemente del juego en sí
- `minetestmapper`: utilidad para generar un mapa desde un mundo de Minetest
- `minetest-mod-*`: *'third-party'* mods (opcionales)  

<br>
## Configuración
Los archivos de configuración de 'Minetest' están en el subdirectorio `~/.minetest`, que se crea apenas se lanza por primera vez el juego.  
Dentro de ese subdirectorio, el archivo `minetest.conf` contiene las preferencias del jugador, que pueden ser modificadas directamente en ese archivo o bien a través del juego.  
En la [*wiki*](https://wiki.minetest.net) del juego se tratan en profundidad estos y otros detalles.  

<br>
## Instalación de 'Minetest' en Debian y derivados
~~~bash
sudo apt-get update
sudo apt install minetest
~~~
Si se desea instalar una versión más actualizada, en la [instalación se puede "invocar" a los 'backports' de Debian](https://hijosdeinit.gitlab.io/howto_minetest_vol01/).  
Multijugador: Es importante que la versión del cliente de 'minetest' que ejecutemos sea compatible con la versión del servidor al que queramos conectarnos.  
<br>
Para lanzar 'Minetest' basta con ejecutar `minetest` desde la terminal, o bien echar mano del menú correspondiente en nuestra distribución GNU Linux.  

<br>
## Comandos de la consola de 'Minetest'
Dentro del juego se pueden ejecutar comandos abriendo la "consola". Las teclas predefinidas para tal efecto -abrir la consola de Minetest- es `/` o `F10`  

Algunos comandos importantes que se pueden ejecutar en la consola del juego son:
~~~
`/status` - Get the status of the server: roster, message of the day.
`/privs` - View privileges.
`/privs <player>` - See privileges 'player'. Requires privilege 'privs'.
`/grant <player> <priv>` - Leave a privilege to 'player'. Requires privilege 'privs'.
`/revoke <player> <priv>` - Remove a privilege to 'player'. Requires privilege 'privs'.
`/time <time>` - Set the time of day. 0 and 24000 correspond to midnight, 12000 and 5000 at noon at dawn. (time * 1000) . Requires privilege 'time'.
`/shutdown` - Turn off the server.
`/setting <name> = <value>` - Adds or replaces a parameter in the configuration file. The parameter can not be applied properly before restarting the server.
`/teleport <x>,<y>,<z>` - Teleport to the indicated position. Requires privilege 'teleport'.
`/grantme <priv>` - Grant yourself privileges. To easily grant yourself all privileges in singleplayer, you might consider running /grantme all
~~~

Por ejemplo:  
Para volar: `grantme fly` (la letra 'k' activa el vuelo)  
Para ir deprisa: `grantme fast` (la letra <span style="background-color:#042206"><span style="color:lime">'j'</span></span> activa el ir deprisa)    
Para garantizarme todos los privilegios: `grantme all`  

<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://wiki.debian.org/Games/Minetest">Debian *wiki*</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
